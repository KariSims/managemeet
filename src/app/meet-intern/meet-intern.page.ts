import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-meet-intern',
  templateUrl: './meet-intern.page.html',
  styleUrls: ['./meet-intern.page.scss'],
})
export class MeetInternPage implements OnInit {


  constructor(private router : Router) { }

  ngOnInit() {
  }

  voirParticipants(){
    this.router.navigate(['list-personnes']);
  }

}
