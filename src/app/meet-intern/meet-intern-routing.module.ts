import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MeetInternPage } from './meet-intern.page';

const routes: Routes = [
  {
    path: '',
    component: MeetInternPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MeetInternPageRoutingModule {}
