import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MeetInternPageRoutingModule } from './meet-intern-routing.module';

import { MeetInternPage } from './meet-intern.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MeetInternPageRoutingModule
  ],
  declarations: [MeetInternPage]
})
export class MeetInternPageModule {}
