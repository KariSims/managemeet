import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreerSalleFormsPage } from './creer-salle-forms.page';

const routes: Routes = [
  {
    path: '',
    component: CreerSalleFormsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreerSalleFormsPageRoutingModule {}
