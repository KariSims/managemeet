import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreerSalleFormsPageRoutingModule } from './creer-salle-forms-routing.module';

import { CreerSalleFormsPage } from './creer-salle-forms.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreerSalleFormsPageRoutingModule
  ],
  declarations: [CreerSalleFormsPage]
})
export class CreerSalleFormsPageModule {}
