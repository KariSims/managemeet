import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UsersService } from '../services/users.service';
import { Salle } from '../model/Salle';
import { Router } from '@angular/router';


@Component({
  selector: 'app-creer-salle-forms',
  templateUrl: './creer-salle-forms.page.html',
  styleUrls: ['./creer-salle-forms.page.scss'],
})
export class CreerSalleFormsPage implements OnInit {

  salle! : Salle;

  constructor(private userService : UsersService,
              private router : Router) { }

  ngOnInit(): void {
    this.salle = new Salle;
  }

  registerMeet(form : NgForm){
    this.userService.createSalleMeet(this.salle)
    .subscribe(data => {
        console.log(data);
        this.router.navigate(['gestion-salle']);
    }
    , error => { return console.log(error); }
    );
  }

}