import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-meet-extern',
  templateUrl: './meet-extern.page.html',
  styleUrls: ['./meet-extern.page.scss'],
})
export class MeetExternPage implements OnInit {

  constructor(private router : Router) { }

  ngOnInit() {
  }

  voirParticipants(){
    this.router.navigate(['list-personnes']);
  }

}