import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MeetExternPage } from './meet-extern.page';

const routes: Routes = [
  {
    path: '',
    component: MeetExternPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MeetExternPageRoutingModule {}
