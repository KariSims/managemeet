import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MeetExternPageRoutingModule } from './meet-extern-routing.module';

import { MeetExternPage } from './meet-extern.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MeetExternPageRoutingModule
  ],
  declarations: [MeetExternPage]
})
export class MeetExternPageModule {}
