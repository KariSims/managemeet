import { Time } from "@angular/common";

export class Salle{
    id?:number;
    numSalle: number;
    dateDispo: Date;
    heure_debut: Time;
    heure_fin: Time;
    statutsalle: string;

}