export class User {
    id?:number;
    username: string;
    email: string;
    password: string;
    direction: string;
    status: string;
}
