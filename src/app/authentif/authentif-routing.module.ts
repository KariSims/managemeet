import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthentifPage } from './authentif.page';

const routes: Routes = [
  {
    path: '',
    component: AuthentifPage
  },
  {
    path: 'inscription',
    loadChildren: () => import('../inscription/inscription.module').then(m => m.InscriptionPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthentifPageRoutingModule {}
