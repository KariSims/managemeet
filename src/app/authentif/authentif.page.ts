import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { UsersService } from '../services/users.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-authentif',
  templateUrl: './authentif.page.html',
  styleUrls: ['./authentif.page.scss'],
})
export class AuthentifPage implements OnInit {

  leStatut : any;

  validations_form: FormGroup;
  matching_passwords_group: FormGroup;
  country_phone_group: FormGroup;

  constructor(public formBuilder: FormBuilder,
    private useServ: UsersService,
    private router: Router) { }

  ngOnInit() {
    this.validations_form = this.formBuilder.group(
      { 'mail' : [null, [Validators.required,Validators.email]],
        'mdp' : [null, [Validators.required]] }
        );
  }

  public onSubmit(form) {
    this.useServ.authentification(form.mail, form.mdp).subscribe(
      (data) => {
        console.log(data.user.status);
        sessionStorage.setItem('username',data.user.username);
        this.leStatut = sessionStorage.setItem('role',data.user.role);

        if(data.user.status == "Administrateur"){
          this.router.navigateByUrl('/folder/Accueil');
        }
        else{
          this.router.navigateByUrl('/gestion-salle');
        }
      },
      (err) => {console.log(err);}
    );
  }

}
