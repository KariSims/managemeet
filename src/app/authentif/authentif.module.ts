import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AuthentifPageRoutingModule } from './authentif-routing.module';

import { AuthentifPage } from './authentif.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    AuthentifPageRoutingModule
  ],
  declarations: [AuthentifPage],
  exports:[RouterModule]
})
export class AuthentifPageModule {}
