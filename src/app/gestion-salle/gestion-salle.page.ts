import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-gestion-salle',
  templateUrl: './gestion-salle.page.html',
  styleUrls: ['./gestion-salle.page.scss'],
})
export class GestionSallePage implements OnInit {

  nameUser : any;

  constructor(private router : Router) {
    this.nameUser = sessionStorage.getItem('username');
  }

  ngOnInit() {}

  optionSalle() {
    this.router.navigateByUrl('/creer-salle-forms'); }
}
