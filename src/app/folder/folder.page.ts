import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
  public folder: string;
  nameUser : any;

  constructor(private activatedRoute: ActivatedRoute,
    public actionSheetController: ActionSheetController,  
    private router: Router        
    ) { 
      this.nameUser = sessionStorage.getItem('username');
    }

  ngOnInit() {
    this.folder = this.activatedRoute.snapshot.paramMap.get('id') ;
  }

  async print() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Choix des réunions',
      buttons: [
        // { text: 'Delete', role: 'destructive' },
        { text: 'Réunion Interne',
          handler: () => {
          this.router.navigateByUrl('/meet-intern');
          }
        },
        { text: 'Réunion Externe',
          handler:()=>{
            this.router.navigateByUrl('/meet-extern');
          }
        },
        { text: 'Ajouter Agent',
          handler:()=>{
            this.router.navigateByUrl('/inscript-agent');
          }
        },
        { text: 'Annuler', role: 'cancel' }
      ]
    });
    
    await actionSheet.present();
  }

}
