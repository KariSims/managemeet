import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InscriptAgentPageRoutingModule } from './inscript-agent-routing.module';

import { InscriptAgentPage } from './inscript-agent.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    InscriptAgentPageRoutingModule
  ],
  declarations: [InscriptAgentPage]
})
export class InscriptAgentPageModule {}
