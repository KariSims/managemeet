import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inscript-agent',
  templateUrl: './inscript-agent.page.html',
  styleUrls: ['./inscript-agent.page.scss'],
})
export class InscriptAgentPage implements OnInit {

  validations_form: FormGroup;

  constructor(public formBuilder: FormBuilder,
              private router: Router
              ) { }

  ngOnInit() {
    this.validations_form = this.formBuilder.group(
      { 'username' : [null, [Validators.required,Validators.email]],
        'mdp' : [null, [Validators.required]],
        'email' : [null, [Validators.required]],
        'drtion' : [null, [Validators.required]] }
      );
  }

}
