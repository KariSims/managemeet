import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InscriptAgentPage } from './inscript-agent.page';

const routes: Routes = [
  {
    path: '',
    component: InscriptAgentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InscriptAgentPageRoutingModule {}
