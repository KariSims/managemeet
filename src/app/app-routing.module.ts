import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/authentif',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'meet-intern',
    loadChildren: () => import('./meet-intern/meet-intern.module').then( m => m.MeetInternPageModule)
  },
  {
    path: 'meet-extern',
    loadChildren: () => import('./meet-extern/meet-extern.module').then( m => m.MeetExternPageModule)
  },
  {
    path: 'inscription',
    loadChildren: () => import('./inscription/inscription.module').then( m => m.InscriptionPageModule)
  },
  {
    path: 'authentif',
    loadChildren: () => import('./authentif/authentif.module').then( m => m.AuthentifPageModule)
  },
  {
    path: 'inscript-agent',
    loadChildren: () => import('./inscript-agent/inscript-agent.module').then( m => m.InscriptAgentPageModule)
  },
  {
    path: 'list-personnes',
    loadChildren: () => import('./list-personnes/list-personnes.module').then( m => m.ListPersonnesPageModule)
  },
  {
    path: 'gestion-salle',
    loadChildren: () => import('./gestion-salle/gestion-salle.module').then( m => m.GestionSallePageModule)
  },
  {
    path: 'creer-salle-forms',
    loadChildren: () => import('./creer-salle-forms/creer-salle-forms.module').then( m => m.CreerSalleFormsPageModule)
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
