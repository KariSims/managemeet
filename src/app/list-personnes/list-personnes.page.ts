import { Component, OnInit } from '@angular/core';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-list-personnes',
  templateUrl: './list-personnes.page.html',
  styleUrls: ['./list-personnes.page.scss'],
})
export class ListPersonnesPage implements OnInit {

  listAgents : Object[];

  constructor(private usersServ : UsersService) {
    this.usersServ.getUsers().subscribe(
       data=>{
         this.listAgents = data;
         console.log(data);
       },
       err=>{
        console.log(err);
       }
    );
  }

  ngOnInit() {
  }

}
