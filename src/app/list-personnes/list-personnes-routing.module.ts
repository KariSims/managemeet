import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListPersonnesPage } from './list-personnes.page';

const routes: Routes = [
  {
    path: '',
    component: ListPersonnesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListPersonnesPageRoutingModule {}
