import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListPersonnesPageRoutingModule } from './list-personnes-routing.module';

import { ListPersonnesPage } from './list-personnes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListPersonnesPageRoutingModule
  ],
  declarations: [ListPersonnesPage]
})
export class ListPersonnesPageModule {}
