import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../model/user';
import { Salle } from '../model/Salle';
import { Observable } from 'rxjs';
import { URLAPI } from '../mesParametres/parametres';

@Injectable({
    providedIn: 'root'
})
export class UsersService {

    constructor(private http: HttpClient) { }

    authentification(username: string, mdp: string) {
        return this.http.post<any>(`${URLAPI}/auth/local`, {
          identifier: username,
          password: mdp
        });
    }

  registerUser(user : User) : Observable<any>{
    return this.http.post<any>(`${URLAPI}/auth/local/register`, user);
  }

  getUsers(){
    return this.http.get<Object[]>(`${URLAPI}/users`);
  }

  /** Recupérer: récupérer un user */
  getOneUser(id?:number){
    return this.http.get<User>(`${URLAPI}/users/${id}`);
  }

   /* Gestion des Salles Meet */
  createSalleMeet(salleMeet : Salle) : Observable<any>{
    return this.http.post<any>(`${URLAPI}/salle-meets`, salleMeet);
  }

}