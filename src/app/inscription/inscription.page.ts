import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UsersService } from '../services/users.service';
import { User } from '../model/user';

@Component({
    selector: 'app-inscription',
    templateUrl: './inscription.page.html',
    styleUrls: ['./inscription.page.scss'],
})

export class InscriptionPage implements OnInit {

    user! : User;

    constructor(private userService: UsersService,
                private router: Router) { }

    ngOnInit(): void {
        this.user = new User;
    }

    public submit(form: NgForm) {
        this.userService.registerUser(this.user)
        .subscribe(data => {
            console.log(data.user);
            this.router.navigate(['authentif']);
        },
        // , error => console.log(error);
        );
    }

}
