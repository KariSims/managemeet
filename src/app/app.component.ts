import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Accueil', url: '/folder/Accueil', icon: 'home' },
    { title: 'Profil', url: '/folder/Profil', icon: 'person-circle' },
    { title: 'Identifiant', url: '/folder/Qrcode', icon: 'qr-code' },
    { title: 'Paramètres et securité', url: '/folder/Paramètres', icon: 'settings' },
    { title: 'Deconnexion', url: '/authentif', icon: 'log-out'}
  ];
  constructor() {}
}
