# ManageMeet

Project de classe de licence Génie Logiciel et Système d'information

# Membres de l'equipes

Classe: Licence3 GLSI B

1- Hadja Aissatou BAH
2- Prince SIMBA ALIMASI 

# Lancement

Au lancement de l'application, l'utilisateur est d'abord soumis à une vérification en :
- Introduisant ses identifiants pour la connexion au cas où il était préalablement enregistré, sinon
- Etant obligé de s'inscrire à la plateforme pour y accéder.

# Inscription

Seuls les administrateurs et les Gestionnaires des salles qui peuvent s'inscrire et ensuite ce sont les Administrateurs qui enregistrent les Agents simples

# Création des réunions

A ce stade, tous les agents peuvent organiser des réunions externes et seuls les Admins organisent celles qui seront internes en fonction des salles mises à leur disposition par les Gestionnaires des salles

# Itinéraire

En fonction du statut choisi dans le formulaire d'inscription (Admin ou Gérant salle) vous êtes dirigé à des pages relatives à ces choix